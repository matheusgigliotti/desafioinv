﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Domain.Models
{
    public abstract class Entidade
    {
        public Guid Id { get; set; }
        public Entidade()
        {
            Id = Guid.NewGuid();
        }
    }
}
