﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Domain.Models
{
    public class Amigo : Entidade
    {
        public string NomeCompleto { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }

        public IEnumerable<Jogo> Jogos { get; set; }
    }
}
