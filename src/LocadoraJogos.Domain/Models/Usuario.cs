﻿using LocadoraJogos.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Models
{
    public class Usuario : Entidade
    {
        public string Login { get; set; }
        public string Senha { get; set; }
        public PerfilEnum Perfil { get; set; }
    }
}
