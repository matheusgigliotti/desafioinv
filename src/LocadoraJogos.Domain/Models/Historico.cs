﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Domain.Models
{
    public class Historico : Entidade
    {
        public Guid JogoId { get; set; }        
        public Guid AmigoId { get; set; }
        public DateTime EmprestadoEm { get; set; }
        public DateTime? DevolvidoEm { get; set; }
        public string Observacao { get; set; }

        public Historico()
        {

        }
        public Historico(Guid idJogo, Guid idAmigo)
        {
            this.JogoId = idJogo;
            this.AmigoId = idAmigo;
            this.EmprestadoEm = DateTime.Now;
        }

        public Jogo Jogo { get; set; }
        public Amigo Amigo { get; set; }
    }
}
