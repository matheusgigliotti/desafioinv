﻿using LocadoraJogos.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Domain.Models
{
    public class Jogo : Entidade
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
        public Guid? IdAmigo { get; set; }

        public Amigo Amigo { get; set; }

        public IEnumerable<Historico> Historico { get; set; }
    }
}
