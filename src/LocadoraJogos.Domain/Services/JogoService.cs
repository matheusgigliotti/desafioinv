﻿using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Interfaces.Services;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Domain.Models.Validations;
using LocadoraJogos.Domain.Notificacoes;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Services
{
    public class JogoService : BaseService, IJogoService
    {
        private readonly IJogoRepository _jogoRepository;
        private readonly IHistoricoRepository _historicoRepository;
        private readonly IConfiguration _configuration;
        private readonly IAmigoRepository _amigoRepository;
        public JogoService(INotificador notificador,
                           IConfiguration configuration,
                           IJogoRepository jogoRepository,
                           IHistoricoRepository historicoRepository,
                           IAmigoRepository amigoRepository) : base(notificador)
        {
            _configuration = configuration;
            _jogoRepository = jogoRepository;
            _historicoRepository = historicoRepository;
            _amigoRepository = amigoRepository;
        }

        public async Task<Jogo> Adicionar(Jogo jogo)
        {
            if (!ExecutarValidacao(new JogoValidation(), jogo)) return null;

            return await _jogoRepository.Adicionar(jogo);
        }

        public async Task<Jogo> Atualizar(Jogo jogo)
        {
            if (!ExecutarValidacao(new JogoValidation(), jogo)) return null;

            return await _jogoRepository.Atualizar(jogo);
        }

        public async Task<bool> Remover(Guid id)
        {
            await _jogoRepository.Remover(id);

            return true;
        }

        public async Task<bool> EmprestarJogo(Guid idJogo, Guid idAmigo)
        {
            var jogo = await _jogoRepository.ObterPorId(idJogo);
            var amigo = await _amigoRepository.ObterPorId(idAmigo);

            if(jogo == null)
            {
                Notificar($"Jogo não existe.");
                return false;
            }

            if(amigo == null)
            {
                Notificar($"Amigo não existe.");
                return false;
            }

            if (jogo.IdAmigo != null)
            {
                Notificar($"Não foi possivel emprestar esse jogo pois ele já esta emprestado.");
                return false;
            }

            var maximoJogosEmprestadoPorAmigo = Convert.ToInt32(_configuration["MaximoJogosEmprestadoPorAmigo"]);
            var totalJogosComAmigo = (await _jogoRepository.Buscar(x => x.IdAmigo == idAmigo)).Count();

            if (totalJogosComAmigo >= maximoJogosEmprestadoPorAmigo)
            {
                Notificar($"Não foi possivel emprestar esse jogo pois o amigo exedeu o limite de jogos emprestados.");
                return false;
            }


            jogo.IdAmigo = idAmigo;
            if (await this.Atualizar(jogo) != null)
                await _historicoRepository.Adicionar(new Historico(jogo.Id, idAmigo));

            return true;
        }

        public async Task<bool> DevolverJogo(Guid idJogo, string observacao)
        {
            var jogo = await _jogoRepository.ObterPorId(idJogo);
            jogo.IdAmigo = null;

            if (await this.Atualizar(jogo) != null)
            {
                var historico = await _historicoRepository.ObterPorIdJogo(idJogo);
                historico.DevolvidoEm = DateTime.Now;
                historico.Observacao = observacao;

                await _historicoRepository.Atualizar(historico);
            }

            return true;
        }
    }
}
