﻿using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Interfaces.Services;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Domain.Models.Validations;
using LocadoraJogos.Domain.Notificacoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Services
{
    public class AmigoService : BaseService, IAmigoService
    {
        private readonly IAmigoRepository _amigoRepository;
        public AmigoService(INotificador notificador,
                            IAmigoRepository amigoRepository) : base(notificador)
        {
            _amigoRepository = amigoRepository;
        }

        public async Task<Amigo> Adicionar(Amigo amigo)
        {
            if (!ExecutarValidacao(new AmigoValidation(), amigo)) return null;

            return await _amigoRepository.Adicionar(amigo);
        }

        public async Task<Amigo> Atualizar(Amigo amigo)
        {
            if (!ExecutarValidacao(new AmigoValidation(), amigo)) return null;

            return await _amigoRepository.Atualizar(amigo);
        }

        public async Task<bool> Remover(Guid id)
        {
            await _amigoRepository.Remover(id);

            return true;
        }
    }
}
