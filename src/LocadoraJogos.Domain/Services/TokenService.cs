﻿using LocadoraJogos.Domain.Interfaces.Services;
using LocadoraJogos.Shared.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Services
{
    public class TokenService : ITokenService
    {

        private readonly string _chaveSecretaTokenJwt;
        private readonly int _duracaoTokenJwt;
        public TokenService(IConfiguration configuration)
        {
            _chaveSecretaTokenJwt = configuration["ConfiguracaoJwt:ChaveSecreta"];
            _duracaoTokenJwt = Convert.ToInt32(configuration["ConfiguracaoJwt:Duracao"]);
        }
        public Task<JwtTokenQuery> GerarTokenJwt(UsuarioLoginQuery usuario)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_chaveSecretaTokenJwt);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuario.Login.ToString()),
                    new Claim(ClaimTypes.Role, usuario.Perfil.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(_duracaoTokenJwt),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);            
            var token = tokenHandler.WriteToken(securityToken);

            var jwtTokenViewModel = new JwtTokenQuery
            {
                Token = token,
                ExpiraEm = securityToken.ValidTo
            };

            return Task.FromResult(jwtTokenViewModel);
        }
    }
}
