﻿using LocadoraJogos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Repositories
{
    public interface IRepository<TEntidade> : IDisposable where TEntidade : Entidade
    {
        Task<TEntidade> Adicionar(TEntidade entity);
        Task<TEntidade> ObterPorId(Guid id);
        Task<List<TEntidade>> ObterTodos();
        Task<TEntidade> Atualizar(TEntidade entity);
        Task Remover(Guid id);
        Task<IEnumerable<TEntidade>> Buscar(Expression<Func<TEntidade, bool>> predicate);
        Task<int> SaveChanges();
    }
}
