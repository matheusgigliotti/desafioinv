﻿using LocadoraJogos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Repositories
{
    public interface IAmigoRepository : IRepository<Amigo>
    {
    }
}
