﻿using LocadoraJogos.Domain.Models;
using LocadoraJogos.Shared.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Repositories
{
    public interface IJogoRepository : IRepository<Jogo>
    {
        Task<Jogo> ObterJogoAmigoAlugado(Guid id);
    }
}
