﻿using LocadoraJogos.Domain.Models;
using LocadoraJogos.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository
    {
        Task<Usuario> RecuperarUsuario(string login, string senha);
    }
}
