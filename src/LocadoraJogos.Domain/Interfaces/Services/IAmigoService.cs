﻿using LocadoraJogos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Services
{
    public interface IAmigoService
    {
        Task<Amigo> Adicionar(Amigo amigo);
        Task<Amigo> Atualizar(Amigo amigo);
        Task<bool> Remover(Guid id);
    }
}
