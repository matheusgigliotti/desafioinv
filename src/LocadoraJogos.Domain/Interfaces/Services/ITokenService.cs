﻿using LocadoraJogos.Shared.Queries;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Services
{
    public interface ITokenService
    {
        Task<JwtTokenQuery> GerarTokenJwt(UsuarioLoginQuery usuario);
    }
}
