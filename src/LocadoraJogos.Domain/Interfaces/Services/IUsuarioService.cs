﻿using LocadoraJogos.Domain.Models;
using LocadoraJogos.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Services
{
    public interface IUsuarioService
    {
        Task<Usuario> Logar(string usuario, string senha);
    }
}
