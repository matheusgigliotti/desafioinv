﻿using LocadoraJogos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Domain.Interfaces.Services
{
    public interface IJogoService
    {
        Task<bool> EmprestarJogo(Guid idJogo, Guid idAmigo);
        Task<bool> DevolverJogo(Guid idJogo, string observacao);
        Task<Jogo> Adicionar(Jogo jogo);
        Task<Jogo> Atualizar(Jogo jogo);
        Task<bool> Remover(Guid id);
    }
}
