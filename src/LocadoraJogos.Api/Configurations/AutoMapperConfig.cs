﻿using AutoMapper;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Shared;
using LocadoraJogos.Shared.Commands;
using LocadoraJogos.Shared.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocadoraJogos.Api.Configurations
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Usuario, UsuarioLoginQuery>();
            CreateMap<Jogo, JogoQuery>().ForMember(x => x.EmprestadoPara, opts => opts.MapFrom(src => src.Amigo.NomeCompleto)).ReverseMap();
            CreateMap<Amigo, AmigoQuery>().ReverseMap();

            CreateMap<CadastroJogoCommand, Jogo>();
            CreateMap<CadastroAmigoCommand, Amigo>();

            CreateMap<EditarJogoCommand, Jogo>();
            CreateMap<EditarAmigoCommand, Amigo>();

            CreateMap<Historico, HistoricoQuery>()
                .ForMember(x => x.Amigo, opts => opts.MapFrom(src => src.Amigo.NomeCompleto))
                .ForMember(x => x.Jogo, opts => opts.MapFrom(src => src.Jogo.Titulo));
        }
    }
}
