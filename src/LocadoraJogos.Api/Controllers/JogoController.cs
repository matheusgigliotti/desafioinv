﻿using AutoMapper;
using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Interfaces.Services;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Domain.Notificacoes;
using LocadoraJogos.Shared.Commands;
using LocadoraJogos.Shared.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocadoraJogos.Api.Controllers
{
    [Authorize]
    public class JogoController : BaseController
    {
        private readonly IJogoRepository _jogoRepository;
        private readonly IJogoService _jogoService;
        private readonly IMapper _mapper;
        private readonly IHistoricoRepository _historicoRepository;

        public JogoController(IJogoRepository jogoRepository, 
                              IJogoService jogoService, 
                              IMapper mapper, 
                              INotificador notificador,
                              IHistoricoRepository historicoRepository) : base(notificador)
        {
            _jogoRepository = jogoRepository;
            _jogoService = jogoService;
            _mapper = mapper;
            _historicoRepository = historicoRepository;
        }

        [HttpGet]
        public async Task<ActionResult> BuscarTodos()
        {
            return CustomResponse(_mapper.Map<IEnumerable<JogoQuery>>(await _jogoRepository.ObterTodos()));
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult> BuscarPorId(Guid id)
        {
            return CustomResponse(_mapper.Map<JogoQuery>(await _jogoRepository.ObterPorId(id)));
        }

        [HttpGet("historico/{id:guid}")]
        public async Task<ActionResult> BuscarHistorico(Guid id)
        {
            return CustomResponse(_mapper.Map<IEnumerable<HistoricoQuery>>(await _historicoRepository.ObterHistoricoPorJogo(id)));
        }

        [HttpPost]
        public async Task<IActionResult> Adicionar(CadastroJogoCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            
            return CustomResponse(_mapper.Map<JogoQuery>(await _jogoService.Adicionar(_mapper.Map<Jogo>(command))));
        }

        [HttpPut]
        public async Task<IActionResult> Atualizar(EditarJogoCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);
           
            return CustomResponse(_mapper.Map<JogoQuery>(await _jogoService.Atualizar(_mapper.Map<Jogo>(command))));
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Excluir(Guid id)
        {
            var jogo = await _jogoRepository.ObterPorId(id);

            if (jogo == null) return NotFound();

            await _jogoService.Remover(id);

            return CustomResponse(jogo);
        }

        [HttpPut("emprestar")]
        public async Task<IActionResult> EmprestarJogo(EmprestarJogoCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            await _jogoService.EmprestarJogo(command.IdJogo, command.IdAmigo);

            return CustomResponse(_mapper.Map<JogoQuery>(await _jogoRepository.ObterJogoAmigoAlugado(command.IdJogo)));

        }

        [HttpPut("devolver")]
        public async Task<IActionResult> DevolverJogo(DevolverJogoCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            await _jogoService.DevolverJogo(command.IdJogo, command.Observacao);

            return CustomResponse(_mapper.Map<JogoQuery>(await _jogoRepository.ObterJogoAmigoAlugado(command.IdJogo)));

        }
    }
}
