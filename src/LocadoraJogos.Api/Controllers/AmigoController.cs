﻿using AutoMapper;
using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Interfaces.Services;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Domain.Notificacoes;
using LocadoraJogos.Shared.Commands;
using LocadoraJogos.Shared.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocadoraJogos.Api.Controllers
{
    [Authorize]
    public class AmigoController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IAmigoRepository _amigoRepository;
        private readonly IAmigoService _amigoService;
        private readonly IHistoricoRepository _historicoRepository;
        public AmigoController(INotificador notificador,
                                  IAmigoRepository amigoRepository,
                                  IAmigoService amigoService,
                                  IHistoricoRepository historicoRepository,
                                  IMapper mapper) : base(notificador)
        {
            _amigoRepository = amigoRepository;
            _amigoService = amigoService;
            _historicoRepository = historicoRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult> BuscarTodos()
        {
            return CustomResponse(_mapper.Map<IEnumerable<AmigoQuery>>(await _amigoRepository.ObterTodos()));
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult> BuscarPorId(Guid id)
        {
            return CustomResponse(_mapper.Map<AmigoQuery>(await _amigoRepository.ObterPorId(id)));
        }

        [HttpGet("historico/{id:guid}")]
        public async Task<ActionResult> BuscarHistoricoPorIdAmigo(Guid id)
        {
            return CustomResponse(_mapper.Map<IEnumerable<HistoricoQuery>>(await _historicoRepository.ObterHistoricoPorAmigo(id)));
        }

        [HttpPost]
        public async Task<IActionResult> Adicionar(CadastroAmigoCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            return CustomResponse(_mapper.Map<AmigoQuery>(await _amigoService.Adicionar(_mapper.Map<Amigo>(command))));
        }

        [HttpPut]
        public async Task<IActionResult> Atualizar(EditarAmigoCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);            

            return CustomResponse(_mapper.Map<AmigoQuery>(await _amigoService.Atualizar(_mapper.Map<Amigo>(command))));
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Excluir(Guid id)
        {
            var jogo = await _amigoRepository.ObterPorId(id);

            if (jogo == null) return NotFound();

            await _amigoService.Remover(id);

            return CustomResponse(jogo);
        }
    }
}
