﻿using AutoMapper;
using LocadoraJogos.Domain.Interfaces.Services;
using LocadoraJogos.Domain.Notificacoes;
using LocadoraJogos.Shared;
using LocadoraJogos.Shared.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LocadoraJogos.Api.Controllers
{
    public class AuthController : BaseController
    {
        private readonly IUsuarioService _usuarioService;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;
        public AuthController(IUsuarioService usuarioService, 
                              ITokenService tokenService, 
                              IMapper mapper, 
                              INotificador notificador) : base(notificador)
        {
            _usuarioService = usuarioService;
            _tokenService = tokenService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Logar(UsuarioLoginCommand command)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuario = _mapper.Map<UsuarioLoginQuery>(await _usuarioService.Logar(command.Usuario, command.Senha));

            if (usuario == null)
            {
                NotificarErro("Usuário ou Senha incorretos");
                return CustomResponse(command);
            }
            return CustomResponse(await _tokenService.GerarTokenJwt(usuario));
        }
    }
}
