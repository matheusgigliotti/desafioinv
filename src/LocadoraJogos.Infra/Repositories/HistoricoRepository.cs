﻿using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Repositories
{
    public class HistoricoRepository : Repository<Historico>, IHistoricoRepository
    {
        public HistoricoRepository(ApplicationDbContext db) : base(db)
        {
        }

        public async Task<IEnumerable<Historico>> ObterHistoricoPorAmigo(Guid idAmigo)
        {
            return await Db.Historicos.AsNoTracking()
               .Include(x => x.Jogo)
               .Include(x => x.Amigo)
               .Where(x => x.AmigoId == idAmigo).ToListAsync();
        }

        public async Task<IEnumerable<Historico>> ObterHistoricoPorJogo(Guid idJogo)
        {
            return await Db.Historicos.AsNoTracking()
               .Include(x => x.Jogo)
               .Include(x => x.Amigo)
               .Where(x => x.JogoId == idJogo).ToListAsync();
        }

        public async Task<Historico> ObterPorIdJogo(Guid idJogo)
        {
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(x => x.JogoId == idJogo && x.DevolvidoEm == null);
        }
    }
}
