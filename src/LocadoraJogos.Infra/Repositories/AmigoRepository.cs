﻿using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Repositories
{
    public class AmigoRepository : Repository<Amigo>, IAmigoRepository
    {
        public AmigoRepository(ApplicationDbContext db) : base(db)
        {
        }
    }
}
