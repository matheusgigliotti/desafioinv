﻿using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Infra.Context;
using LocadoraJogos.Shared.Queries;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Repositories
{
    public class JogoRepository : Repository<Jogo>, IJogoRepository
    {
        public JogoRepository(ApplicationDbContext db) : base(db) { }

        public async Task<Jogo> ObterJogoAmigoAlugado(Guid id)
        {
            return await Db.Jogos.AsNoTracking()
                .Include(c => c.Amigo)
                .FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}
