﻿using LocadoraJogos.Domain.Interfaces.Repositories;
using LocadoraJogos.Domain.Models;
using LocadoraJogos.Infra.Context;
using LocadoraJogos.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Repositories
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(ApplicationDbContext context) : base(context) { }

        public async Task<Usuario> RecuperarUsuario(string login, string senha)
        {
            return (await Buscar(x => x.Login == login && x.Senha == senha)).FirstOrDefault();
        }
    }
}
