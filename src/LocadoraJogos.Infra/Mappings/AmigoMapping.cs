﻿using LocadoraJogos.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Mappings
{
    public class AmigoMapping : IEntityTypeConfiguration<Amigo>
    {
        public void Configure(EntityTypeBuilder<Amigo> builder)
        {
            builder.ToTable("Amigos");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.NomeCompleto)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(x => x.Endereco)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(x => x.Telefone)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.HasMany(x => x.Jogos)
                .WithOne(x => x.Amigo)
                .HasForeignKey(x => x.IdAmigo);
        }
    }
}
