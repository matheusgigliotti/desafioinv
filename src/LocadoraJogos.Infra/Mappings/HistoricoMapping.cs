﻿using LocadoraJogos.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Mappings
{
    public class HistoricoMapping : IEntityTypeConfiguration<Historico>
    {
        public void Configure(EntityTypeBuilder<Historico> builder)
        {
            builder.ToTable("Historicos");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.JogoId)
                .IsRequired();

            builder.Property(x => x.AmigoId)
                .IsRequired();

            builder.Property(x => x.EmprestadoEm)
                .IsRequired();

            builder.Property(x => x.Observacao)
                .HasColumnType("varchar(150)");

            //builder.has(x => new { x.JogoId, x.AmigoId });
        }
    }
}
