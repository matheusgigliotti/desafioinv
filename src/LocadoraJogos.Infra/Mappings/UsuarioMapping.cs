﻿using LocadoraJogos.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Mappings
{
    public class UsuarioMapping : IEntityTypeConfiguration<Usuario>
    {

        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuarios");
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.Login)
                .IsRequired();

            builder.Property(x => x.Senha)
                .IsRequired();

            builder.Property(x => x.Perfil)
                .IsRequired()
                .HasConversion<string>();

            builder.HasData(new Usuario
            {
                Id = Guid.NewGuid(),
                Login = "admin",
                Senha = "admin", //invillia
                Perfil = Domain.Enums.PerfilEnum.Admin
            });
        }
    }
}
