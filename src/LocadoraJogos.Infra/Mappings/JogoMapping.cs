﻿using LocadoraJogos.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocadoraJogos.Infra.Mappings
{
    public class JogoMapping : IEntityTypeConfiguration<Jogo>
    {
        public void Configure(EntityTypeBuilder<Jogo> builder)
        {
            builder.ToTable("Jogos");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Titulo)
                .IsRequired()
                .HasColumnType("varchar(100)");


            builder.Property(x => x.Descricao)
                .IsRequired()
                .HasColumnType("varchar(150)");

            builder.Property(x => x.Tipo)
                .IsRequired()
                .HasConversion<string>();
        }
    }
}
