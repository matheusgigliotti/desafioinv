﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Commands
{
    public class DevolverJogoCommand
    {
        public Guid IdJogo { get; set; }
        public string Observacao { get; set; }
    }
}
