﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Commands
{
    public class EmprestarJogoCommand
    {
        public Guid IdJogo { get; set; }
        public Guid IdAmigo { get; set; }
    }
}
