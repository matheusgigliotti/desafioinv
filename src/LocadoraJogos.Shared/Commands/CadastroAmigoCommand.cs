﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Commands
{
    public class CadastroAmigoCommand
    {
        public string NomeCompleto { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
    }
}
