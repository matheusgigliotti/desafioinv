﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Commands
{
    public class EditarJogoCommand
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
    }
}
