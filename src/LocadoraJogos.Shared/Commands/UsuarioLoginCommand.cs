﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared
{
    public class UsuarioLoginCommand
    {
        public string Usuario { get; set; }
        public string Senha { get; set; }
    }
}
