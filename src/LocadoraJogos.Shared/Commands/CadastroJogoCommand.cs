﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Commands
{
    public class CadastroJogoCommand
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
    }
}
