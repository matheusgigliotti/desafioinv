﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Queries
{
    public class JwtTokenQuery
    {
        public string Token { get; set; }
        public DateTime ExpiraEm { get; set; }
        public bool IsValid
        {
            get
            {
                if (!string.IsNullOrEmpty(Token))
                    return DateTime.Now < ExpiraEm;
                else
                    return false;
            }
        }
    }
}
