﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Queries
{
    public class UsuarioLoginQuery
    {
        public string Login { get; set; }
        public string Perfil { get; set; }
    }
}
