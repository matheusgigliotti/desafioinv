﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Queries
{
    public class HistoricoQuery
    {
        public string Jogo { get; set; }
        public string Amigo { get; set; }
        public DateTime EmprestadoEm { get; set; }
        public DateTime? DevolvidoEm { get; set; }
        public string Observacao { get; set; }
    }
}
