﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Queries
{
    public class JogoQuery
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
        public string EmprestadoPara { get; set; }
        public bool Emprestado => !string.IsNullOrEmpty(EmprestadoPara);
    }
}
