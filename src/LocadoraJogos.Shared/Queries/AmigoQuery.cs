﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocadoraJogos.Shared.Queries
{
    public class AmigoQuery
    {
        public Guid Id { get; set; }
        public string NomeCompleto { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
    }
}
