#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
COPY ["/wait-for-it.sh", "wait-for-it.sh"]
RUN chmod +x wait-for-it.sh
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["src/LocadoraJogos.Api/LocadoraJogos.Api.csproj", "src/LocadoraJogos.Api/"]
COPY ["src/LocadoraJogos.Shared/LocadoraJogos.Shared.csproj", "src/LocadoraJogos.Shared/"]
COPY ["src/LocadoraJogos.Infra/LocadoraJogos.Infra.csproj", "src/LocadoraJogos.Infra/"]
COPY ["src/LocadoraJogos.Domain/LocadoraJogos.Domain.csproj", "src/LocadoraJogos.Domain/"]
RUN dotnet restore "src/LocadoraJogos.Api/LocadoraJogos.Api.csproj"
COPY . .
WORKDIR "/src/src/LocadoraJogos.Api"
RUN dotnet build "LocadoraJogos.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "LocadoraJogos.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "LocadoraJogos.Api.dll"]